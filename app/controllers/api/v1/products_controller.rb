class Api::V1::ProductsController < ApplicationController
  # GET /products
  # GET /products.json
  def index
    products = Product.all
    render json: products, status: 200
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    product = Product.find(params[:id])
    if product.update(product_params)
      render json: product, status: 200
    else
      render json: 'Error occurred!', status: 500
    end
  end

  # GET /products/check_price
  # GET /products/check_price.json
  def check_price
    products = Product.find_records(product_params[:product_list])
    if products.present?
      render json: { total: Api::DiscountCalculator.call(products) }, status: 200
    else
      render json: 'No records found!', status: 404
    end
  end

  private

  # Strong permitted/whitelistd params
  def product_params
    params.require(:products).permit(:price, product_list: [])
  end
end
