class ApplicationController < ActionController::API
  rescue_from(ActiveRecord::RecordNotFound) { |exception| render_error(exception, 404) }

  protected

  # For handeling exception raised when no record is found
  def render_error(exception, status)
    render json: exception, status: status
  end
end
