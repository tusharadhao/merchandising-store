class Discount < ApplicationRecord
  belongs_to :product
  enum discount_type: { singular_discount: 0, multiple_dicount: 1 }
end
