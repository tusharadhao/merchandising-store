class Product < ApplicationRecord
  has_many :discounts

  # Find the Product records for the provided code_list array
  def self.find_records(code_list = nil)
    unique_records = where(code: code_list)
    code_list&.map { |code| unique_records.find { |product| product.code == code } }&.compact
  end
end
