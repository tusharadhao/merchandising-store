# frozen_string_literal: true

module Api
  class DiscountCalculator
    attr_reader :products

    def initialize(products)
      @products = products
    end

    def self.call(*args, &block)
      new(*args, &block).total_discounted_price
    end

    # Evaluate total price for the products
    def total_discounted_price
      discounted_price_list = products.uniq.each_with_object([]) do |product, prices|
        prices << discount_price(product) if product.discounts.present?
      end
      discount_price = discounted_price_list&.sum || 0
      subtotal = products.sum(&:price)

      subtotal - discount_price
    end

    # returns total discount price
    def discount_price(product)
      prices = product.discounts.each_with_object([]) do |discount, prices_array|
        products_count = products.select { |record| record.id == product.id }&.count
        if products_count && products_count >= discount.min_quantity
          prices_array << calculate_percentage(discount, product)
        end
      end

      prices.compact&.sum || 0
    end

    # returns calculated discount value as per the discount type
    def calculate_percentage(discount, product)
      if discount.singular_discount?
        (product.price.to_f * discount.percent_amount.to_f / 100)
      else
        (product.price.to_f * discount.min_quantity.to_f * discount.percent_amount.to_f / 100)
      end
    end
  end
end
