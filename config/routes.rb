Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :products, via: %i[get put patch] do
        get :check_price, on: :collection
      end
    end
  end
end
