require 'rails_helper'

RSpec.describe Product, type: :request do
  fixtures :products, :discounts

  describe 'GET /index' do
    before do
      get '/api/v1/products'
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'returns proper records' do
      products = Product.all

      expect(products.count).to eql(2)
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      it 'returns updated product price' do
        product = Product.first
        patch api_v1_product_path(id: product.id), params: { products: { price: 20 } }

        expect(response).to have_http_status(:success)
        expect(product.reload.price).to eql(20)
      end
    end

    context 'with invalid parameters' do
      it 'returns 404 http status' do
        patch api_v1_product_path(id: '123'), params: {}

        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'GET /check_price' do
    context 'with valid product_list' do
      let(:product_code1) { Product.first.code }
      let(:product_code2) { Product.last.code }

      it 'returns total price of the products' do
        get '/api/v1/products/check_price', params: { products: { product_list: [product_code1, product_code2] } }

        expect(response).to have_http_status(:success)
        expect(JSON.parse(response.body)['total']).to eql('19.0')
      end
    end

    context 'with empty product_list' do
      it 'returns record being empty message' do
        get '/api/v1/products/check_price', params: { products: { product_list: [] } }

        expect(response).to have_http_status(404)
        expect(response.body).to eql('No records found!')
      end
    end
  end

  describe 'GET /check_price' do
    context 'with valid product_list' do
      let(:product_code1) { Product.first.code }
      let(:product_code2) { Product.last.code }

      context 'for singular discount type' do
        it 'returns total value with discounted price of the products' do
          get '/api/v1/products/discounted_price',
              params: { products: { product_list: [product_code1, product_code1, product_code2] } }

          expect(response).to have_http_status(:success)
          expect(JSON.parse(response.body)['total']).to eql('19.0')
        end
      end

      context 'for multiple discount type' do
        it 'returns total value with discounted price of the products' do
          get '/api/v1/products/discounted_price',
              params: { products: { product_list: [product_code1, product_code2, product_code2, product_code2] } }

          expect(response).to have_http_status(:success)
          expect(JSON.parse(response.body)['total']).to eql('30.0')
        end
      end
    end

    context 'with empty product_list' do
      it 'returns record being empty message' do
        get '/api/v1/products/check_price', params: { products: { product_list: [] } }

        expect(response).to have_http_status(404)
        expect(response.body).to eql('No records found!')
      end
    end
  end
end
